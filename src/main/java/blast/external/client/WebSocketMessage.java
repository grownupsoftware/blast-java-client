package blast.external.client;

public class WebSocketMessage {
    private byte[] byteMessage;
    private String stringMessage;
    private byte opcode;

    public WebSocketMessage(byte[] message) {
        this.byteMessage = message;
        this.opcode = ExternalClient.OPCODE_BINARY;
    }

    public WebSocketMessage(String message) {
        this.stringMessage = message;
        this.opcode = ExternalClient.OPCODE_TEXT;
    }

    public boolean isText() {
        return opcode == ExternalClient.OPCODE_TEXT;
    }

    public boolean isBinary() {
        return opcode == ExternalClient.OPCODE_BINARY;
    }

    public byte[] getBytes() {
        return byteMessage;
    }

    public String getText() {
        return stringMessage;
    }
}
