package blast.external.client;

public interface ThreadInitializer {
   void setName(Thread t, String name);
}
