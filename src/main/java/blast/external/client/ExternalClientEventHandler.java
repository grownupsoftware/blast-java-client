package blast.external.client;

public interface ExternalClientEventHandler {

    public void onOpen();

    public void onMessage(WebSocketMessage message);

    public void onClose();

    public void onError(WebSocketException e);

    public void onLogMessage(String msg);
}
