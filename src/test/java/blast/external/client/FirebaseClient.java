package blast.external.client;

import blast.external.client.WebSocketException;
import blast.external.client.ExternalClientEventHandler;
import blast.external.client.WebSocketMessage;
import blast.external.client.ExternalClient;
import java.net.URI;
import java.util.concurrent.Semaphore;

/**
 * User: greg
 * Date: 7/25/13
 * Time: 6:48 PM
 *
 * This test is just a quick smoke test to make sure we can connect to a wss endpoint.
 */
public class FirebaseClient {

    private ExternalClient client;
    private Semaphore semaphore;

    private class Handler implements ExternalClientEventHandler {

        @Override
        public void onOpen() {
            System.out.println("Opened socket");
            client.close();
        }

        @Override
        public void onMessage(WebSocketMessage message) { }

        @Override
        public void onClose() {
            System.out.println("Closed socket");
            semaphore.release(1);
        }

        @Override
        public void onError(WebSocketException e) {
            e.printStackTrace();
        }

        @Override
        public void onLogMessage(String msg) {
            System.err.println(msg);
        }
    }

    public void start() throws WebSocketException, InterruptedException {
        semaphore = new Semaphore(0);
        URI uri = URI.create("wss://gsoltis.firebaseio-demo.com/.ws?v=5");
        client = new ExternalClient(uri);
        client.setEventHandler(new Handler());
        client.connect();
        semaphore.acquire(1);
        client.blockClose();
    }

    public static void main(String[] args) {
        try {
            new FirebaseClient().start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
